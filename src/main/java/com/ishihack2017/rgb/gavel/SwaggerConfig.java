package com.ishihack2017.rgb.gavel;

import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket documentation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).build()
                .useDefaultResponseMessages(false)
                .directModelSubstitute(ResponseEntity.class, String.class)
                .pathMapping("/").apiInfo(metadata());
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Ishinomaki Hackathon 2017 RGB REST API")
                .description("石巻ハッカソン RGB チーム REST APIサービス.")
                .version("1.0").build();
    }

    @Bean
    public UiConfiguration uiConfig() {
        val submitMethods = new String[] {"get", "post", "put", "delete", "patch", "head"};
        return new UiConfiguration(null, submitMethods);
    }
}

package com.ishihack2017.rgb.gavel.repository;

import com.ishihack2017.rgb.gavel.entity.Summary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SummaryRepository.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Repository
public interface SummaryRepository extends JpaRepository<Summary, String> {
}

package com.ishihack2017.rgb.gavel.repository;

import com.ishihack2017.rgb.gavel.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * BrandRepository.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Repository
public interface BrandRepository extends JpaRepository<Brand, String> {
}

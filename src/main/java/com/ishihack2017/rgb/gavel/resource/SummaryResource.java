package com.ishihack2017.rgb.gavel.resource;

import com.ishihack2017.rgb.gavel.entity.Summary;
import com.ishihack2017.rgb.gavel.repository.SummaryRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * SummaryResource.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@RestController
@RequestMapping("api/summary") @Slf4j @RequiredArgsConstructor
public class SummaryResource {

    // TODO: Service化する
    private final SummaryRepository repository;

    @GetMapping("today") @SneakyThrows
    public List<Summary> findSummaries() {
        return repository.findAll();
    }
}

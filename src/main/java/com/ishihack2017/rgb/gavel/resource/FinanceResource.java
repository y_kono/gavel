package com.ishihack2017.rgb.gavel.resource;

import com.ishihack2017.rgb.gavel.entity.Slice;
import com.ishihack2017.rgb.gavel.entity.Trend;
import com.ishihack2017.rgb.gavel.service.FinanceService;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * FinanceResource.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@RestController @RequestMapping("api/finance") @Slf4j
public class FinanceResource {

    @Autowired private FinanceService service;

    @GetMapping("pull")
    public List<Slice> pullData(
            @NonNull @RequestParam String brand,
            @NonNull @RequestParam String market,
            @RequestParam Integer interval,
            @RequestParam String term) throws UnirestException {
        return service.pullData(brand, market, interval, term);
    }

    @GetMapping("nikkei_trend")
    public Trend nikkeiTrend() {
        return service.nikkeiTrend();
    }
}

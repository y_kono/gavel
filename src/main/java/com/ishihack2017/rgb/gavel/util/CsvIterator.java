package com.ishihack2017.rgb.gavel.util;

import com.orangesignal.csv.io.CsvEntityReader;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.IOException;
import java.util.Iterator;

/**
 * CsvIterator.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Slf4j
public class CsvIterator<E> implements Iterator<E>, Iterable<E>, AutoCloseable {

    private final CsvEntityReader<E> reader;

    private E next;

    public static <E> CsvIterator<E> of(CsvEntityReader<E> reader) {
        return new CsvIterator<>(reader);
    }

    @SneakyThrows
    private CsvIterator(CsvEntityReader<E> reader) {
        this.reader = reader;
        next = reader.read();
    }

    @Override @SneakyThrows
    public boolean hasNext() {
        return next != null;
    }

    @Override
    public E next() {
        val now = next;
        next = readNext();
        return now;
    }

    @Override
    public void close() {
        try {
            reader.close();
        } catch (IOException e) {
            log.warn(e.getMessage(), e);
        }
    }

    @SneakyThrows
    private E readNext() {
        for (;;) {
            try {
                return reader.read();
            } catch (IndexOutOfBoundsException | NumberFormatException ignore) {
                // Windows OS などで保存された場合、終端制御コードが含まれ例外が発生するので、そのときは無視する.
            }
        }
    }

    @Override
    public Iterator<E> iterator() {
        return this;
    }
}
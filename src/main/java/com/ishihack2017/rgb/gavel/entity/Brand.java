package com.ishihack2017.rgb.gavel.entity;

import com.orangesignal.csv.annotation.CsvColumn;
import com.orangesignal.csv.annotation.CsvEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Brand.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@CsvEntity @Entity
@Data @NoArgsConstructor @AllArgsConstructor @Builder(toBuilder = true)
public class Brand {

    @Id @CsvColumn(position = 0)
    private String code;

    @CsvColumn(position = 1)
    private String name;

    @CsvColumn(position = 2)
    private String industry;
}

package com.ishihack2017.rgb.gavel.entity;

import lombok.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Slice.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Data @NoArgsConstructor @AllArgsConstructor @Builder(toBuilder = true)
public class Slice {

    private String code;

    private Date dateTime;

    private BigDecimal open;

    private BigDecimal close;

    private BigDecimal high;

    private BigDecimal low;

    private BigDecimal volume;

    @SuppressWarnings("unused")
    public String getDateTimeFormatted() {
        if (dateTime == null) return null;
        val format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.JAPAN);
        return format.format(dateTime);
    }
}

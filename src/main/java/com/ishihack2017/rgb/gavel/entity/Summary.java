package com.ishihack2017.rgb.gavel.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Summary.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @Builder(toBuilder = true)
public class Summary {

    @Id
    private String brandCode;

    private String brandName;

    private BigDecimal stockValue;

    private String industry;

    private BigDecimal rangeAverage;

    private BigDecimal volumeWeightedAveragePrice;
}

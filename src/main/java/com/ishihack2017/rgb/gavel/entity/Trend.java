package com.ishihack2017.rgb.gavel.entity;

import lombok.*;

import java.math.BigDecimal;

/**
 * Trend.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Data @NoArgsConstructor @AllArgsConstructor @Builder(toBuilder = true)
public class Trend {

    private BigDecimal currentClose;

    private BigDecimal rangeAverage;

    @SuppressWarnings("unused")
    public BigDecimal getDifference() {
        return currentClose.subtract(rangeAverage);
    }

    @SuppressWarnings("unused")
    public BigDecimal getRatio() {
        return currentClose.divide(rangeAverage,10, BigDecimal.ROUND_HALF_UP);
    }
}

package com.ishihack2017.rgb.gavel.service;

import com.ishihack2017.rgb.gavel.entity.Brand;
import com.ishihack2017.rgb.gavel.entity.Slice;
import com.ishihack2017.rgb.gavel.entity.Summary;
import com.ishihack2017.rgb.gavel.entity.Trend;
import com.ishihack2017.rgb.gavel.repository.BrandRepository;
import com.ishihack2017.rgb.gavel.repository.SummaryRepository;
import com.ishihack2017.rgb.gavel.util.CsvIterator;
import com.mashape.unirest.http.Unirest;
import com.orangesignal.csv.CsvConfig;
import com.orangesignal.csv.CsvReader;
import com.orangesignal.csv.io.CsvEntityReader;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * FinanceService.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Service @Slf4j @RequiredArgsConstructor
public class FinanceService {

    private static final int DAY_AS_SECONDS = 24 * 60 * 60;

    private final ResourceLoader loader;

    private final BrandRepository brandRepository;

    private final SummaryRepository summaryRepository;

    @PostConstruct
    public void loadSummary() {
        importFromCsv();
        loadSummaries();
    }

    @SneakyThrows
    private void importFromCsv() {
        val resource = loader.getResource("classpath:data/nikkei225-stock-prices.csv");
        val csvConfig = new CsvConfig(',', '"', '"');
        try (   val inReader = new InputStreamReader(resource.getInputStream(), "UTF-16LE");
                val csvRecords = CsvIterator.of(
                        CsvEntityReader.newInstance(new CsvReader(inReader, csvConfig), Brand.class))
        ) {
            brandRepository.save(csvRecords);
        } catch (IOException e) {
            log.error("ファイルの読み込みに失敗.", e);
            throw e;
        }
    }

    private void loadSummaries() {
        val brands =brandRepository.findAll();
        brands.stream()
                .map(brand -> {
                    val slices = pullData(brand.getCode(), "TYO", DAY_AS_SECONDS, "20d");
                    return Summary.builder()
                            .brandCode(brand.getCode())
                            .brandName(brand.getName())
                            .stockValue(slices.get(0).getClose())
                            .industry(brand.getIndustry())
                            .rangeAverage(calcRangeAverage(slices))
                            .volumeWeightedAveragePrice(calcVolumeWeightedAveragePrice(slices))
                            .build();
                })
                .forEach(summaryRepository::save);
    }

    public Trend nikkeiTrend() {
        val slices = pullData( "NI225","INDEXNIKKEI", DAY_AS_SECONDS, "20d");

        return Trend.builder()
                .currentClose(slices.get(0).getClose())
                .rangeAverage(calcRangeAverage(slices))
                .build();
    }

    @SneakyThrows
    public List<Slice> pullData(String brandCode, String marketCode, Integer interval, String term) {
        if (interval == null) interval = 86400;
        if (term == null) term = "1M";
        return parse(Unirest.get("https://www.google.com/finance/getprices")
                .queryString("q", brandCode)
                .queryString("x", marketCode)
                .queryString("i", interval)
                .queryString("p", term)
                .queryString("f", "d,c,v,o,h,l")
                .queryString("df", "cpct")
                .queryString("auto", "1")
                .queryString("ei", "4rrIWJHoIYya0QS1i4IQ")
                .queryString("ts", new Date().getTime())
                .asString().getRawBody(), brandCode, interval);
    }

    @SneakyThrows
    private List<Slice> parse(InputStream body, String brandCode, int interval) {
        try (val reader = new BufferedReader(new InputStreamReader(body))) {
            val baseDateTime = new Date();
            return reader.lines()
                    .map(this::urlDecode)
                    .filter(line -> !line.matches("^.+=.*$"))
                    .map(line -> line.split(","))
                    .map(columns -> {
                        // aで始まる場合はUNIX TIME, それ以外は前者からの差分
                        long time;
                        if (columns[0].startsWith("a")) {
                            time = Long.valueOf(columns[0].substring(1)) * 1000;
                            baseDateTime.setTime(time);
                        } else {
                            time = baseDateTime.getTime() + Integer.valueOf(columns[0]) * interval * 1000;
                        }

                        return Slice.builder()//DATE,CLOSE,HIGH,LOW,OPEN,VOLUME
                                .dateTime(new Date(time))
                                .close(new BigDecimal(columns[1]))
                                .high(new BigDecimal(columns[2]))
                                .low(new BigDecimal(columns[3]))
                                .open(new BigDecimal(columns[4]))
                                .volume(new BigDecimal(columns[5]))
                                .code(brandCode)
                                .build();
                    })
                    .sorted(comparing(Slice::getDateTime).reversed())
                    .collect(Collectors.toList());
        }
    }

    @SneakyThrows
    private String urlDecode(String text) {
        return URLDecoder.decode(text, "UTF-8");
    }

    private BigDecimal calcRangeAverage(List<Slice> slices) {
        BigDecimal closeSum = BigDecimal.ZERO;
        for (val slice : slices) {
            closeSum = closeSum.add(slice.getClose());
        }
        val size = new BigDecimal(slices.size());
        return closeSum.divide(size, 10, BigDecimal.ROUND_UP);
    }

    private BigDecimal calcVolumeWeightedAveragePrice(List<Slice> slices) {
        BigDecimal closeVolumeSum = BigDecimal.ZERO;
        BigDecimal volumeSum = BigDecimal.ZERO;
        for (val slice : slices) {
            val closeVolume = slice.getClose().multiply(slice.getVolume());
            closeVolumeSum = closeVolumeSum.add(closeVolume);
            volumeSum = volumeSum.add(slice.getVolume());
        }
        return closeVolumeSum.divide(volumeSum, 10, BigDecimal.ROUND_UP);
    }
}

# deploy command.
```bash
# Pivotalにログイン
$ cf login -a api.run.pivotal.io

# アプリケーションのビルド
$ ./gradlew clean build

# CFへデプロイ -b java_buildpack でビルドパック指定できる。（省略可）
$ cf push golden-hammer -p build/libs/gavel-0.0.1-SNAPSHOT.jar -m 512m

# スケールアウト
$ cf scale golden-hammer -i 2

# アプリの状態確認
$ cf app golden-hammer
$ cf logs golden-hammer
$ cf logs golden-hammer --recent

# Blue Green Deploy
$ cf push golden-hammer-v2 -p build/libs/gavel-0.0.1-SNAPSHOT.jar -m 512m -k 512m -b java_buildpack
$ cf map-route golden-hammer-v2 cfapps.io -n golden-hammer
$ cf unmap-route golden-hammer cfapps.io -n golden-hammer
$ cf unmap-route golden-hammer-v2 cfapps.io -n golden-hammer-v2
$ cf stop golden-hammer
$ cf delete golden-hammer

# SSH login
$ cf ssh-enable golden-hammer
$ cf ssh golden-hammer
# /home/vcap/app がカレント。 /home/vcap/app/logs/application.log でログを表示できる。

```
